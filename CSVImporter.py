import numpy as np

def create_molecular_matrix(filename, NUMBER_OF_MOLECULES_TO_LOAD):
    text_file = open(filename, "r")
    stripped_row = []
    molecules = []

    for line in text_file:
        row = line.split(",")
        stripped_row.append([s.rstrip() for s in row])


    for k in range(2, 2 + NUMBER_OF_MOLECULES_TO_LOAD):
        molecules.append(np.zeros((3, 7)))
        count = -1
        column = 0
        for i in range(2, 29):
            if (i - 1) % 4 != 0:
                count += 1
                if count == 2:
                    molecules[k - 2][count][column] = stripped_row[k][i]
                    column += 1
                    count = -1
                else:
                    molecules[k - 2][count][column] = stripped_row[k][i]

    return molecules

if __name__ == "__main__":

    molecular_matrix = create_molecular_matrix("RawData.csv",100)
    print(molecular_matrix[99])






