import numpy as np
import random


def square_coulomb(configs,atoms,coulomb):
    """
    Function that takes our input flat matrix for each configuration
    and squares each element for use later in calculation of norms
    
    :param configs:  Number of configurations
    :param atoms: Dimensions of coulomb matrix, atoms squared is our length of flat matrix
    :param coulomb: Input flat matrix for all configurations
    :return: coulomb with each element squared 
    """


    coulomb_squared = np.zeros((configs,atoms**2))

    for i in range(0, configs):

        for j in range(0,atoms**2):

            coulomb_squared[i][j]=coulomb[i][j]**2.0

    return coulomb_squared


def find_norms(configs,atoms,coulomb_squared):
    """
    Function that calculates a column vector with length atoms
    Each row in of the column vector corresponds to a row 
    in our coulomb matrix for a given configuration
    Row i corresponds to the elements from 7i to 7i+6 in our flat matrix
    The values stored in the column vector for one configuration
    are the square roots of the sum of the squared elements per row i
    
    :param configs: Number of configurations
    :param atoms: Dimensions of coulomb matrix, atoms squared is our length of flat matrix
    :param coulomb_squared: Original coulomb matrix with each element squared
    :return: Norm column vector
    """

    norms = np.zeros((atoms, configs))

    for i in range(0,configs):

         for j in range (0,((atoms**2)-(atoms-1)),atoms):

             row_sum = 0.0

             for k in range(0,atoms):

                 x = coulomb_squared[i][j+k]
                 row_sum = row_sum + x

             index = int(j/atoms)
             norms[index][i] = row_sum**0.5

    return norms


def noise_norms(configs,atoms,norms,sigma):
    """
    Adding a random number between 0 and sigma (chosen to be 1)
    to each element in the column vector norm for each configuration
    
    :param configs: Number of configurations
    :param atoms: Dimensions of coulomb matrix, atoms squared is our length of flat matrix
    :param norms: Column vector of norms
    :param sigma: Range of random numbers
    :return: Norm column vector with noise added to each element
    """

    for i in range(0, configs):

        for j in range(0, atoms):

            norms[j][i] = norms[j][i] + random.uniform(0,sigma)

    return norms

def sort_norms(norms,configs,atoms):
    """
    The noisy column vectors are sorted from highest value at the top 
    to lowest at the bottom
    
    :param norms: Noisy norms
    :param configs: Number of configurations
    :param atoms: Dimensions of coulomb matrix, atoms squared is our length of flat matrix
    :return: Noisy norm column vector sorted from highest to lowest
    """

    output = np.zeros((configs, atoms))
    sort_map = np.zeros((configs,atoms))
    norms = np.transpose(norms)

    for i in range(0,configs):

        temp = np.zeros((configs, atoms))
        temp = norms[i][:]
        sort_map = np.argsort(norms, axis=1)[::-1]
        temp = sorted(temp,reverse=True)
        output[i][:] = temp


    return output, sort_map


def random_coulomb(coulomb):
    """
    Main module that takes an input dataset of coulomb matrices 
    for number of configurations
    :param coulomb: Input dataset
    :return: random_coulomb: Randomly sorted coulomb matrices for every configuration
    """
    sizes = coulomb.shape
    configs = sizes[0]
    atoms = int((sizes[1])**0.5)
    sigma = 1.0
    coulomb_squared = square_coulomb(configs, atoms, coulomb)
    norms = find_norms(configs, atoms, coulomb_squared)
    norms = noise_norms(configs,atoms,norms,sigma)
    after_sort, sort_map = sort_norms(norms, configs, atoms)
    coulomb = np.reshape(coulomb, (configs, atoms, atoms))

    for i in range(0, configs):

        coulomb[i] = coulomb[i, sort_map[i]]

    coulomb = np.reshape(coulomb, (configs, atoms * atoms))

    return coulomb

def multiple_matrices(coulomb,Nitter):
    """
    Generating multiple replications of random coulomb matrices per configuration
    
    :param coulomb: Input dataset
    :param Nitter: Number of replications
    :return: A joined larger dataset containing  
    """

    a = random_coulomb(coulomb)


    for i in range(0,Nitter-1):

        b = random_coulomb(coulomb)
        total = np.concatenate((a, b))
        a = total

    return total



if __name__ == "__main__":

    coulomb = np.zeros((300,49))

    # for i in range(0,3):
    #
    #     for j in range(0,9):
    #
    #         coulomb[i][j] = random.uniform(0.1,4.0)
    #
    #
    # print("before",coulomb)
    # result = random_coulomb(coulomb)
    # print("after",result)
    Nitter = 5
    test = multiple_matrices(coulomb,Nitter)

if __name__ == "__main__":

    coulomb = np.zeros((300,49))

    # for i in range(0,3):
    #
    #     for j in range(0,9):
    #
    #         coulomb[i][j] = random.uniform(0.1,4.0)

    #
    # print("before",coulomb)
    # result = random_coulomb(coulomb)
    # print("after",result)

    test = multiple_matrices(coulomb)
    print(test.shape)

