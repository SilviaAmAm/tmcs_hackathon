# Neural Networks and potential energy surfaces

In this exercise, you will use neural networks to fit a correction to the potential energy surface of a chemical reaction. The reaction under study is:

CH4 + CN.  -> CH3. + HCN

In ab-initio MD, generally DFT is the only electronic structure calculation affordable. However, it would be good to have energies and forces as accurate as if they were calculated with methods like coupled cluster, but without the additional computational cost. In this exercise, you will use neural networks to fit the *difference* between two energies calculated with two distinct electronic structure methods. If this works, one could use a cheap level of theory in MD calculations and use this `correction surface' to calculate more accurate energies.

## General information about the exercise

### The raw data

The data consists in a .csv file which contains 7288 xyz configurations of the system and their energies (in Hartree) calculated with PBE and B3LYP. Each line is one configuration, with the atom labels followed by its xyz coordinates. There are 7 atoms in the system: 2xC, 4xH and 1xN. At the end of the same line are the two values of the energies.

The first line of the file contains some labels, that you can ignore. The first number for each sample is just numbering the samples.


### The descriptor

In order to use the data to train a neural network, the xyz coordinates cannot be used directly, they have to be turned in a suitable form. The descriptor that we will use is called the Coulomb matrix. You can find more information about the Coulomb matrix in this paper:

[Coulomb Matrix](http://pubs.acs.org/doi/abs/10.1021/ct400195d)

There are many ways of using the Coulomb matrix, and we will focus on the eigenspectrum of the Coulom matrix and the sorted Coulomb matrix.



### Division of tasks

This is a suggestion of how you could divide the tasks among 5 people in the group. However, you are free to divide it in which ever way you prefer!

1. One person could write the scripts to import the data from the CSV files into the program. 
1. One person could write a program that takes the data already extracted from the CSV files and computes the "original" Coulomb matrix.
1. One person could generate the eigenspectrum of the Coulomb matrix.
1. One person could generate the sorted version of the Coulomb matrix.
1. The last person could construct the machine learning pipeline using sci-kit learn.

What you need to make sure is that you communicate to each other in what form you need the data. In this way, that the person that writes the code on which you depend will know in what format the output of his module needs to be!
