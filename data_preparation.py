"""
This script utilises all the modules written during the hackathon.
It will use the coulomb matrix in three forms to predict the energy difference between two DFT elevels of theory
"""

import CSV_Module
import coulomb
import CSV
import numpy as np
import sorted_matrix
import CSV_Energy_Module
import Learn as LL
import random_coulomb
import eigenval
import plot_results
import os.path

import sklearn.metrics as metrics

# import 3Dexample

# Number of samples to load
Nsamples = 7288

# Import data from the CSV file
molecular_matrix = CSV.create_molecular_matrix("RawData.csv",Nsamples)
energy_list = CSV.create_energy_matrix("RawData.csv",Nsamples)

# Generate the coulomb matrix
coulomb_matrices = coulomb.turn_square_to_line(molecular_matrix)


# Generate the coulomb matrix
coulomb_matrices = np.asarray(coulomb_matrices)


# arranged_coulomb_matrices = sorted_matrix.sort_full_matrix(coulomb_matrices)  # Ordered matrix


# Nrep=5
# energy_list = np.tile(energy_list,Nrep) # Repeat energy five times (to match the repetition of randomly sorted matrices)
# arranged_coulomb_matrices = random_coulomb.multiple_matrices(coulomb_matrices,Nrep)    # Random matrix

arranged_coulomb_matrices = eigenval.eigenval_processor(coulomb_matrices)     # eigenvalues of the coulomb matrix




# Rocco's learning bit
l = LL.Learn(arranged_coulomb_matrices,np.array(energy_list),0.1)

NN_fname="NN_eigen.sav"

if(not os.path.isfile(NN_fname)):
    l.pipeline(pca=False)
    l.train(HLS=[43, 44, 45, 46, 47],LRI=np.linspace(0.0001,0.5,5),ALPHA=np.linspace(0.1,0.3,5))
    l.save(NN_fname)
else:
    l.load(NN_fname)

# This is predicting the energies for some values in the total set
# E_predicted = l.predict(arranged_coulomb_matrices[0::100,:])
# print("R2 for some values in the total set:",metrics.r2_score(energy_list[0::100],E_predicted))
# plot_results.plot_results(energy_list[0::100],E_predicted)

# This is predicting the energies for values in the test set
E_predicted = l.predict(l.get_dataT())
print("R2 for the values in the test set:",metrics.r2_score(l.get_valuesT(),E_predicted))
# plot_results.plot_results(l.get_valuesT(),E_predicted)
plot_results.plotSeaborn(l.get_valuesT(),E_predicted)

# E_predicted = l.predict(arranged_coulomb_matrices[2465:5836,:])
# print("R2:",metrics.r2_score(energy_list[2465:5836],E_predicted))
# plot_results.plot_results(energy_list[2465:5836],E_predicted)

def outliers_idx(r,E1,E2):
    idx = []

    for i in range(E1):
        if(abs(E1[i]-E2[i]) > r):
            idx.append(i)

    return idx