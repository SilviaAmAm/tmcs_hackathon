"""""
This module contains the functions required to generate the coulomb matrix from the set of xyz coordinates of each
molecules' configuration.

"""

import numpy as np

def inter_atom_distance(vec1,vec2):
    dist = np.sqrt((vec1[0]-vec2[0])**2+(vec1[1]-vec2[1])**2+(vec1[2]-vec2[2])**2)
    return dist

def diagonal_element(charge):
    return 0.5*charge**(2.4)

def off_diagonal_element(charge1,charge2,inter_atom_distance):
    result = (charge1*charge2)/(inter_atom_distance)
    return result

def charge(num):
    if num==0 or num==5: # Charge on Carbon
        return 6.0
    elif num == 6: # Charge on Nitrogen
        return 7.0
    else: # Charge on Hydrogen
        return 1.0

def calculate_CoulombMatrix(molecular_matrix):
    """
    :param molecular_matrix: a 3x7 matrix of atomic coordinates
    :return: coulomb matrix corresponding to that configuration
    """
    test_matrix = np.transpose(molecular_matrix)
    coulomb_matrix = np.zeros(shape=(7, 7))

    for i in range(7):
        for j in range(7):
            r = inter_atom_distance(test_matrix[i], test_matrix[j])
            if i == j:
                coulomb_matrix[i][j] = diagonal_element(charge(i))
            else:
                coulomb_matrix[i][j] = off_diagonal_element(charge(i), charge(j), r)

    return coulomb_matrix

def turn_square_to_line(molecular_matrix):
    massive_matrix = []
    for k in range(len(molecular_matrix)):
        temp = calculate_CoulombMatrix(molecular_matrix[k])

        a  = []
        for i in range(7):
            for j in range(7):
                a.append(temp[i][j])
        massive_matrix.append(a)
    return massive_matrix


if __name__ == "__main__" :
    import CSV_Module
    molecular_matrix = CSV_Module.create_molecular_matrix("RawData.csv", 3)
    print(len(molecular_matrix))
    #coulomb_matrix = calculate_CoulombMatrix(molecular_matrix[2])
    #print(coulomb_matrix)
    final_matrix = turn_square_to_line(molecular_matrix)
    print(final_matrix)