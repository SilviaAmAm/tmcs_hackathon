"""
Module to plot the results of the NN output. Input is the exact and the predicted energy list 
Plots a simple scatter graph

Tim Burd
"""


import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
# plt.style.use('ggplot')
import numpy as np

def plot_results(list1, list2):
    fig2, ax2 = plt.subplots(figsize=(8, 7))
    ax2.scatter(list1, list2, marker="o", c="r")
    ax2.set_xlabel('Electronic structure energy corrections (Ha)')
    ax2.set_ylabel('NN energy corrections (Ha)')
    ax2.legend()
    plt.show()

def plotSeaborn(list1, list2):
    """
    :param list1: List of Original values
    :param list2: List of NN predicted values
    :return: None
    """

    df = pd.DataFrame()
    df['High level calculated energies (Ha)'] = list1
    df['NN predicted energies (Ha)'] = list2
    lm = sns.lmplot('High level calculated energies (Ha)','NN predicted energies (Ha)', data=df, scatter_kws={"s": 20, "alpha": 0.6}, line_kws={"alpha":0.5})
    lm.set(ylim=(1.78, 1.90))
    lm.set(xlim=(1.78, 1.90))

    sns.plt.show()




if __name__ == '__main__':
    list1 = [1, 5, 3, 2, 6, 8, 3, 5, 7, 9]
    list2 = [1, 5, 3, 2, 6, 8, 3, 5, 7, 9]

    plot_results(list1, list2)