import pandas as pd
import numpy as np

df = pd.read_csv("RawData.csv")
print(df.head())

output = open("geometries.xyz", "w")

for index, row in df.iterrows():
    output.write("7\n")
    output.write("comment line\n")

    #print(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7])

    for i in range(7):
        output.write(str(row[4*i+1]) + "\t" + str(row[4*i+2]) + "\t" + str(row[4*i+3]) + "\t" + str(row[4*i+4]) + "\n")



output.close()