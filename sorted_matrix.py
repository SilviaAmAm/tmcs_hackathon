"""
This module can be used to convert a set of matricies (in the form of a numpy array with easch row representing each 
matrix, into a set of sorted matricies.
This means the modulus of the rows and lengths of the matrix are in order.
The main function to use here is sort_full_matrix
The rest are functions contained within this function
NOTE this assumes the input matricies are SQUARE
Have fun!

Tim Burd
TMCS Software development course 19/5/17
"""


import numpy as np

def sort_rows(vector):

    """This takes a single matrix in the form of a vector, 
    and sorts it such that the moduluses of its ROWS are in order"""

    width = vector.size     #The length of the vector (total number of entries in the array)
    dimension = int((width)**(0.5))   # Side lenght of the square matrix

    # Generate a list of the moduli of each row
    modulus = []
    for i in range(dimension):
        modulus.append(0)
        for j in range(dimension):
            index = (dimension*i) + j
            modulus[i] = modulus[i] + (vector[0][index])**2

    # Initialise the output vector
    sorted_vector = np.zeros((1,width))

    # Fill the output vector with rows in the correct order
    for i in range(dimension):
        biggest = np.argmax(modulus) #returns index of biggest modulus
        sorted_vector[0][i*dimension:(i+1)*dimension] = vector[0][dimension*biggest:(dimension*(biggest+1))] # Adds this row to sorted_vector
        modulus[biggest] = 0 #turns this row off

    return sorted_vector




def sort_collums(vector):

    """This takes a single matrix in the form of a vector, 
    and sorts it such that the moduluses of its COLLUMNS are in order"""

    width = vector.size     #The length of the vector (total number of entries in the array)
    dimension = int((width)**(0.5))    # Side lenght of the square matrix


    # Generate a list of the moduli of each row
    modulus = []
    for i in range(dimension):
        modulus.append(0)
        for j in range(dimension):
            index = i + j*dimension
            modulus[i] = modulus[i] + (vector[0][index])**2


    # Initialise the output vector
    sorted_vector = np.zeros((1,width))

    # Fill the output vector with rows in the correct order
    for i in range(dimension):
        biggest = np.argmax(modulus) #returns index of biggest modulus
        for j in range(dimension):
            sorted_vector[0][j*dimension + i] = vector[0][(j*dimension)+biggest]
        modulus[biggest] = 0 #turns this row off

    return sorted_vector


def sort_full_vector(vector):

    """ This function will sort a matrix (in the form of a vector) such that its 
    ROWS AND COLLUMNS are in order. NOTE for a symmetric matrix, such as the coulomb matrix"""

    intemediate = sort_rows(vector)
    final = sort_collums(intemediate)
    return final


def sort_full_matrix(input_matrix):

    """This function accepts a numpy array of matricies in vector form
    It returns the set of sorted matricies.
    THIS IS THE MAIN FUNCTION OF THIS MODULE"""

    sizes = input_matrix.shape
    rows = sizes[0]


    # Initialise output array
    output = np.zeros(sizes)

    # Fill in the output row by row
    for i in range(rows):
        temp = []
        temp.append(input_matrix[i][:])
        result = sort_full_vector(np.asarray(temp))
        output[i] = result[0]
    return output


if __name__ == '__main__':


    test1 = np.asarray([[1, 2, 3, 4, 5, 6, 7, 8, 9]])
    result1 = sort_full_matrix(test1)
    print(result1)
    # EXPECT [9, 8, 7, 6, 5, 4, 3, 2, 1]

    test2 = np.asarray([[1, 2, 3, 4, 5, 6, 7, 8, 9],
                        [1, 2, 3, 4, 5, 6, 7, 8, 9],
                        [1, 2, 3, 4, 5, 6, 7, 8, 9]])
    result2 = sort_full_matrix(test2)
    print(result2)
    # EXPECT [[9, 8, 7, 6, 5, 4, 3, 2, 1],
     #           [9, 8, 7, 6, 5, 4, 3, 2, 1],
    #           [9, 8, 7, 6, 5, 4, 3, 2, 1]]

    test3 = np.zeros((10, 9))
    result3 = sort_full_matrix(test3)
    print(result3)
    # EXPECT a matrix of zeros

    test4 = np.random.rand(99,9)
    result4 = sort_full_matrix(test4)
    print(result4)


    test5 = np.asarray([[1, 2, 1, 2, 1, 2, 1, 2, 1]])
    result5 = sort_full_matrix(test5)
    print(result5)
    # EXPECT [[1, 2, 2, 2, 1, 1, 2, 1, 1]