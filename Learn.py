"""
The class Learn set the following pipeline for the MLPRegressor:
> Standardise data
> (OPTIONAL) Principal component analysis (PCA)
> Neuronal Network regression

The pipeline is set with the pipeline function. The can principal component analysis is added to the pipeline.
The neuronal network parameters (hidden_layer_sizes,learning_rate_init,alpha) are optimised with the function
train. The neuronal network with the best parameters is finally trained and tested.
After the neuronal network is trained, predictions on new data points can be made with the function predict.

---

19.05.2017 - Created by Rocco Meli (rocco.meli@chem.ox.ac.uk)
"""

import numpy as np
import matplotlib.pylab as plt
from sklearn import preprocessing as preproc
from sklearn import decomposition as deco
from sklearn import model_selection as modsel
from sklearn import pipeline as pip
from sklearn import neural_network as nn
from sklearn import externals as ext
import sys

class Learn:

    def __init__(self,data,values,test_size=0):
        # Split training set if required
        if(test_size == 0):
            self.data = data
            self.values = values
            self.test = False
        else:
            self.data, self.dataT, self.values, self.valuesT = modsel.train_test_split(
                data, values, test_size=test_size, random_state=0)
            self.test = True

        # Store size of training and test sets
        self.train_idx = (0,len(self.data)-1)
        self.test_idx =  (len(self.data),len(data)-1)

        # Store data size
        self.Nsamples = data.shape[0]
        self.Nfeatures = data.shape[1]

        # Print info of data set
        print("> Samples:", self.Nsamples)
        print("> Features:", self.Nfeatures)

        # Keep track of pipeline
        self.pipe = None

        # Keep track of estimation
        self.estimated = False

        # Keep track of PCA
        self.pca = False

        # Keep track of loading
        self.loaded = False

    def get_dataT(self):
        """
        Returns the descriptor of the samples in the test set
        :return: shape (number of samples, number of features), type: float
        """
        return self.dataT

    def get_valuesT(self):
        """
        Returns the energies of the samples in the test set
        :return: shape (number of samples,1), type: float
        """
        return self.valuesT

    def plot_features(self):
        """
        Plot mean and std of a feature over all samples
        """
        plt.errorbar(range(self.Nfeatures),self.data.mean(axis=0),self.data.std(axis=0),fmt='o')
        plt.title('Feature mean and variance')
        plt.xlabel('Feature index')
        plt.show()

    def pipeline(self,pca=False):
        """
        Create pipeline
        > Standardise data
        > (OPTIONAL) Principal component analysis (PCA)
        > Neuronal Network regression
        
        PCA: Optimise number of features by PCA (boolean)
        """

        print("Generating pipeline...",end=" ")
        sys.stdout.flush()

        # Keep track of PCA
        self.pca = pca

        # Define pipeline steps
        steps = []

        # Add scaler to the pipeline
        steps.append(('scale',preproc.StandardScaler()))

        # Add pca (optionally)
        if pca:
            steps.append(('pca',deco.PCA()))

        # Add Neuronal Network regression
        steps.append(('NN',nn.MLPRegressor(max_iter=5000,
                                           activation='tanh')))
        # Generate pipeline
        self.pipe = pip.Pipeline(steps=steps)

        print("done")

    def train(self,HLS,LRI,ALPHA,components=None,Nsplits=5):
        """
        Estimate best parameters (see below)
        
        HLS: List of number of neurons per hidden layers (integers)
        LRI: List of initial learning rates (float)
        ALPHA: List of values for the regularisation term alpha (float)
        Components: List of number of components for PCA (float)
        Nsplits: Size of data set split for KFold (integers)
        """

        print("Estimating...",end=' ')
        sys.stdout.flush()

        # Check pipeline
        if self.pipe is None:
            print("\nERROR: Pipeline not set.")
            print("In function",self.estimate.__name__)
            sys.exit(-1)

        # Check if arguments are consistent
        #   PGA requires components
        if(components is None and self.pca==True):
            print("\nERROR: Components is needed if PCA is requested.")
            print("         In function", self.estimate.__name__)
            sys.exit(-1)
        elif(components is not None and self.pca==False):
            print("\nWARNING: Components is set but PCA is not requested.")
            print("         In function", self.estimate.__name__)

        # Set iterator for model parameters
        cv_iter = modsel.KFold(n_splits=Nsplits)

        # Dictionary of parameters to optimize
        # Key should be "NAME-IN-PIPE__PARAMETER-NAME"
        params_to_optimise = {}

        # If PCA is required
        if(self.pca):
            # Define range of n_components
            params_to_optimise.update({'pca__n_components':components})


        # Define range of hidden_layer_sizes
        params_to_optimise.update({'NN__hidden_layer_sizes':HLS})

        # Define range of learning_init_rate
        params_to_optimise.update({'NN__learning_rate_init':LRI})

        # Define range of learning_init_rate
        params_to_optimise.update({'NN__alpha': ALPHA})

        # Create estimator for given parameters (use GridSearchCV)
        self.estimator = modsel.GridSearchCV(self.pipe,params_to_optimise,cv=cv_iter)

        # Fit estimator to data
        self.estimator.fit(self.data,self.values.ravel())

        print("done")

        print('Best accuracy {:4.2f}% '.format(100 * self.estimator.best_score_), end='')
        print('was achieved with: \n{}'.format(self.estimator.best_params_))

        if self.test:
            test_score = self.estimator.score(self.dataT, self.valuesT)
            print('Score on test data: {:4.2f}%'.format(test_score * 100))

        # Set pipeline with optimal parameters
        self.set_optimal_pipe()

        # Keep track of function call
        self.estimated = True

    def set_optimal_pipe(self):
        """
        Set pipeline with optimal parameters after estimation
        """

        print("Setting pipe with optimal values...", end=' ')
        sys.stdout.flush()

        # Dictionary of best parameters
        best_params = {}

        # Best number of PCA components
        if(self.pca):
            best_params.update({'pca__n_components':self.estimator.best_params_['pca__n_components']})

        # Best number of neurons for the hidden layer
        best_params.update({'NN__hidden_layer_sizes':self.estimator.best_params_['NN__hidden_layer_sizes']})

        # Best initial learning rate
        best_params.update({'NN__learning_rate_init': self.estimator.best_params_['NN__learning_rate_init']})

        # Best regularisation parameter
        best_params.update({'NN__alpha': self.estimator.best_params_['NN__alpha']})

        # Set best parameters
        self.pipe.set_params(**best_params)

        # Refit data with best parameter, ready for prediction
        self.pipe.fit(self.data,self.values.ravel())

        print('done')


    def predict(self,data):
        """
        Predict values for given data using the trained neurola network
        """

        # Check parameters have been estimated
        if not (self.estimated == False or self.loaded == False):
            print("\nERROR: Optimal parameters not estimated or not loaded.")
            print("In function", self.predict.__name__)
            sys.exit(-1)

        # Predict new values with new data points
        return self.pipe.predict(data)

    def save(self,fname="NN.sav"):
        """
        Saving optimised neuronal network
        """
        # Check parameters have been estimated
        if not self.estimated:
            print("\nERROR: Optimal parameters not estimated.")
            print("In function", self.predict.__name__)
            sys.exit(-1)

        # Dump pipeline object into binary file
        ext.joblib.dump(self.pipe, fname)

    def load(self,fname="NN.sav"):
        """
        Loading optimised neuronal network
        """

        # Load pipeline from binary file
        self.pipe = ext.joblib.load(fname)

        # Keep track of leading
        self.loaded = True


if __name__ == "__main__":
    # Test
    # Apply estimation to a function with random noise
    from numpy import random as rd

    # Generate noisy data
    x = np.linspace(-10,10,10000)
    y = np.sin(x)  + rd.normal(0,0.4,10000)

    # Plot noisy data
    plt.plot(x,y, "og")

    # Create Learn object
    l = Learn(x.reshape(len(x),1),y.reshape(len(y),1),1)

    # Define pipeline (without PCA)
    l.pipeline(pca=False)

    # Estimate optimal parameters
    l.train(HLS=[(i,) for i in range(100,101)],LRI=np.linspace(0.1,1,5),ALPHA=np.logspace(-5,-3,5))

    # Save model
    #l.save()

    # # Load model
    # l.load()

    # Generate new coordinates
    x = np.linspace(-10,10,100)

    # Predict parabola for new coordinates
    y_predicted = l.predict(x.reshape(len(x),1))

    # Plot predicted values
    plt.plot(x,y_predicted, "or")
    plt.show()