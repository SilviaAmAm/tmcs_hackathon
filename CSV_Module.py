import numpy as np

def create_molecular_energy_matrix(filename, NUMBER_OF_MOLECULES_TO_LOAD):
    text_file = open(filename, "r")
    stripped_row = []
    molecules = []

    for line in text_file:
        row = line.split(",")
        stripped_row.append([s.rstrip() for s in row])

    for k in range(1, 1+ NUMBER_OF_MOLECULES_TO_LOAD):
        molecules.append(np.zeros(2))

        molecules[k-1][0] = stripped_row[k][29]
        molecules[k - 1][1] = stripped_row[k][30]

    return np.asarray(molecules)


