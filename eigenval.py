#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 18 11:47:18 2017

@author: cha10vd
"""
import numpy as np
import numpy.linalg as linalg
import math

def eigenval_processor(vector_list):

    mat_dim = int(math.sqrt(vector_list.shape[1]))
    
    eigval_list = np.zeros((vector_list.shape[0],mat_dim))
    
    for i in range(0,vector_list.shape[0]):
        matrix = vector_list[i,:].reshape(mat_dim, mat_dim)
        
        eigenvals = eigenval_calc(matrix)
        eigval_list[i,:] = eigenvals 
                   
    return(eigval_list)
        
def eigenval_calc(matrix):
    

    verbosity = 0 #set verbosity to 0 to omit all debugging output

     # Calculate the eigenvalues and eigenvectors

    eigenvals, eigenvecs = linalg.eig(matrix)

     # Sort the eigenvalues, by definition, this is in ascending order

    eigenvals = np.sort(eigenvals, axis=0)

     # flip the vector, so that it's in descending order

    eigenvals = np.flipud(eigenvals)


    if(verbosity == 1):

         print("The eigenvalues for our matrix are:\n\n", eigenvals, "\n")
         #print("The eigenvectors for our matrix are:\n\n", eigenvecs, "\n")
         print("The sorted eigenvalues for our matrix, in ascending order, are:\n\n", eigenvals, "\n")

    return(eigenvals)


if __name__ == "__main__":
    
    print("main")

    vector_list = np.zeros((4,9)) 

    print()
    
    for i in range(0,vector_list.shape[0]):
        vector_list[i,:] = np.arange(9)
    
    eigval_list = eigenval_processor(vector_list)
    
    print(eigval_list)