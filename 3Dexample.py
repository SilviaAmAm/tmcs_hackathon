import pygame
import numpy as np
import CSV
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *



def inter_atom_distance(vec1,vec2):
    dist = np.sqrt((vec1[0]-vec2[0])**2+(vec1[1]-vec2[1])**2+(vec1[2]-vec2[2])**2)
    return dist

def min_distance(coordinates,carbon_vect):
    distances=[]
    for t in range(1,5):
        dist = inter_atom_distance(coordinates[t],carbon_vect)
        distances = np.append(distances,dist)
    return np.amin(distances), np.argmin(distances)+1

def which_edges(coordinates):
    c1_coordinates = coordinates[0]
    c2_coordinates = coordinates[5]
    min_c2_dist = min_distance(coordinates, c2_coordinates)
    print(min_c2_dist[0])
    index_h = min_c2_dist[1]
    c1_dist = inter_atom_distance(coordinates[index_h], c1_coordinates)
    print(c1_dist)
    if c1_dist < min_c2_dist[0]:
        print("CH4")
        return 0
    else:
        print("CH3")
        return 5

molecular_matrix = CSV.create_molecular_matrix("RawData.csv",2000)
trial_coordinates = np.transpose(molecular_matrix[910])
carbon = which_edges(trial_coordinates)

verticies = (
    (trial_coordinates[0][0], trial_coordinates[0][1], trial_coordinates[0][2]), #C1
    (trial_coordinates[1][0], trial_coordinates[1][1], trial_coordinates[1][2]),   #H1
    (trial_coordinates[2][0], trial_coordinates[2][1], trial_coordinates[2][2]),  #H2
    (trial_coordinates[3][0], trial_coordinates[3][1], trial_coordinates[3][2]),  #H3
    (trial_coordinates[4][0], trial_coordinates[4][1], trial_coordinates[4][2]),  #H4
    (trial_coordinates[5][0], trial_coordinates[5][1], trial_coordinates[5][2]),  #C2
    (trial_coordinates[6][0], trial_coordinates[6][1], trial_coordinates[6][2]), #N
    )

edges = (
    (5,6),
    (0,1),
    (carbon,2),
    (0,3),
    (0,4),
    )

def Cube():
    glBegin(GL_LINES)
    for edge in edges:
        for vertex in edge:
            glVertex3fv(verticies[vertex])
    glEnd()

def main():
    pygame.init()
    display = (1000,800)
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)
    gluPerspective(90, (display[0]/display[1]), 0.1, 50.0)
    gluLookAt(0.5,0.5,1.0,-0.6,0,0,0,1,1.0)
    glTranslatef(-6.0,-2.0,-5)
    pygame.display.set_caption("Energies")

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        glRotatef(0.5, 6, 0, 0)
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        Cube()
        pygame.display.flip()
        pygame.time.wait(10)

if __name__ == "__main__":
    main()