import numpy as np

def create_molecular_energy_matrix(filename, NUMBER_OF_MOLECULES_TO_LOAD):
    text_file = open(filename, "r")
    stripped_row = []
    molecules = []

    for line in text_file:
        row = line.split(",")
        stripped_row.append([s.rstrip() for s in row])

    for k in range(1, 1+ NUMBER_OF_MOLECULES_TO_LOAD):
        temp = float(stripped_row[k][30])-float(stripped_row[k][29])
        molecules.append(temp)

    return np.asarray(molecules)



